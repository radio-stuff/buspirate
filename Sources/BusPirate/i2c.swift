//
//  i2c.swift
//  BusPirate.swift
//
//  Created by William Dillon on 7/28/20.
//

import Foundation
import PromiseKit

public func doesThisWork() -> Promise<Int> {
    return Promise { seal in
        seal.fulfill(1)
    }
}

/// Bus pirate i2c clock frequency settings.  These values have the control nibble (0x60) already applied
public enum BusPirateI2CSpeed: UInt8 {
    case khz400 = 0x63
    case khz100 = 0x62
    case khz50  = 0x61
    case khz5   = 0x60
}

public extension BusPirate {
    func i2cSetSpeed(speed: BusPirateI2CSpeed) -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPending: return
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: speed.rawValue)
                self.state = .i2cPending
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    func i2cStartBit() -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPending: return
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: 0x02)
                self.state = .i2cPending
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    func i2cStopBit() -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPending: return
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: 0x03)
                self.state = .i2cPending
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    /// Read one byte from the i2c bus.  This version uses promises to implement the result handling.
    /// It should be automatically selected by the compiler using the type interfence system when a
    /// variable is set
    /// - Returns: Promise for the resulting value
    func i2cReadByte() -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPendingRead: return
                case .i2cByteAvailable:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: 0x04)
                self.state = .i2cPendingRead
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    /// Send an i2c ACK
    /// - Throws: SerialHandler error, or `BusPirateError.invalidMode`
    func i2cSendACK() -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPending: return
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: 0x06)
                self.state = .i2cPending
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    /// Send an i2c NACK
    /// - Throws: SerialHandler error, or `BusPirateError.invalidMode`
    func i2cSendNACK() -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cPending: return
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                try serialPort.send(byte: 0x07)
                self.state = .i2cPending
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    /// Higher-level i2c data write.  This method handles a complete i2c write transaction, including the start
    /// and stop bits.
    /// - Parameter data: Data to write to the device
    /// - Returns: Promise with a variety of error conditions, including low-level serial issues to invalid
    ///    modes or unexpected results.  A successful result will return a BusPirateState that contains the
    ///    result from the i2c transaction (nack or ack) for each byte.
    func i2cWriteData(_ data: Data) -> Promise<BusPirateStates> {
        return i2cStartBit().then { _ in
            self.i2cWriteDataLL(Data(data))
        }.then { state -> Promise<(BusPirateStates, BusPirateStates)>  in
            self.acknowledge()
            return self.i2cStopBit().map { (state, $0) }
        }.then { (states: (BusPirateStates, BusPirateStates)) -> Promise<BusPirateStates> in
            return Promise<BusPirateStates>.value(states.0)
        }
    }

    /// Low-level write data maps 1:1 with the BusPirate write data command.  This command requires that
    /// you do an i2c start bit and stop bit on either end of the command.
    /// - Parameter data: Data to write to the device
    /// - Returns: Promise with a variety of error conditions, including low-level serial issues to invalid
    ///    modes or unexpected results.  A successful result will return a BusPirateState that contains the
    ///    result from the i2c transaction (nack or ack) for each byte.
    func i2cWriteDataLL(_ data: Data) -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            guard data.count <= 16 else {
                seal.reject(BusPirateError.invalidLength)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cWritePending, .i2cWritingData: return
                case .i2cWriteComplete:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                // The value to send to the bus pirate is length - 1, because there's
                // no point in sending zero bytes.  Also, the bitfield length is only
                // 4 bits, so you want to make the most of it.
                let count = UInt8(data.count) - 1

                let packet: [UInt8] = [(0x10 + count)] + [UInt8](data)

                try serialPort.send(array: packet)
                self.state = .i2cWritePending(data.count)
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    func i2cWriteRead(writeData data: Data, count: Int) -> Promise<BusPirateStates> {
        self.acknowledge()
        return Promise { seal in
            // Ensure that we're in an appropriate mode
            guard self.state == .i2c else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            guard data.count <= 4096 else {
                seal.reject(BusPirateError.invalidLength)
                return
            }

            guard count <= 4096 else {
                seal.reject(BusPirateError.invalidLength)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2cWriteReadPending, .i2cWriteRead: return
                case .i2cDataAvailable:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                // The packet structure of this command is 0x08, then number of written
                // bytes in a UInt16 (big endian), then the number of bytes to read in
                // a UInt16 (big endian), then the contents of the data.
                let writeCount = UInt16(data.count)
                let readCount  = UInt16(count)

                let packet: [UInt8] = [
                    0x08,
                    UInt8(writeCount >> 8 & 0x00FF), UInt8(writeCount & 0x00FF),
                    UInt8( readCount >> 8 & 0x00FF), UInt8( readCount & 0x00FF)
                ] + [UInt8](data)

                try serialPort.send(array: packet)
                self.state = .i2cWriteReadPending(count)
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }

    func i2cReadRegisterByte(i2cAddress: UInt8, register: UInt8) -> Promise<UInt8> {
        let i2cPacket = Data([
            i2cAddress << 1, // i2c peripheral of device
            register         // Set the register for access
        ])

        acknowledge()
        return self.i2cWriteData(i2cPacket).then { _ -> Promise<BusPirateStates> in
            self.acknowledge()
            return self.i2cWriteRead(writeData: Data([i2cAddress << 1 + 1]), count: 1)
        }.then { state -> Promise<UInt8> in
            switch state {
            case let .i2cDataAvailable(data):
                guard let byte = [UInt8](data).first else {
                    throw BusPirateError.invalidLength
                }
                return Promise<UInt8> { $0.fulfill(byte) }
            default: throw BusPirateError.unexpectedResult
            }
        }
    }

    func i2cReadRegister(i2cAddress: UInt8, register: UInt8, count: Int) -> Promise<Data> {
        let i2cPacket = Data([
            i2cAddress << 1, // i2c peripheral of device
            register         // Set the register for access
        ])

        acknowledge()
        return self.i2cWriteData(i2cPacket).then { _ in
            self.i2cWriteRead(writeData: Data([i2cAddress << 1 + 1]), count: count)
        }.then { state -> Promise<Data> in
            switch state {
            case let .i2cDataAvailable(data):
                return Promise<Data> { $0.fulfill(data) }
            default: throw BusPirateError.unexpectedResult
            }
        }
    }

    func i2cWriteRegister(i2cAddress: UInt8, register: UInt8, value: UInt8) -> Promise<Bool> {
        let i2cPacket = Data([
            i2cAddress << 1, // i2c peripheral of device
            register,        // Set the register for access
            value            // Value to set
        ])

        acknowledge()

        return i2cWriteData(i2cPacket).then { state -> Promise<Bool> in
            switch state {
            case let .i2cWriteComplete(acks):
                guard let ack = acks.first else {
                    throw BusPirateError.invalidLength
                }
                return Promise<Bool> { $0.fulfill(ack) }
            default: throw BusPirateError.unexpectedResult
            }
        }
    }
}
