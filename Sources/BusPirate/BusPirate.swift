/* StateMachine.swift
 *
 * Created by William Dillon on 7/27/2020
 * Copyright Housedillon Inc. All rights reserverd
 *
 */

import SerialHandler
import RingBuffer
import Foundation
import PromiseKit

public class BusPirate: BusPirateProtocol {
    /// This lock protects the BusPirate.  If it's locked, then the bus pirate is in the middle of an operation
    internal var operationLock = NSLock()

    /// This callback is used to implement the promises.  If there's an existing callback, then we are in the
    /// middle of an operation, and it's not safe to start another.
    internal var stateChangeCallback: ((BusPirateStates, BusPirateStates) -> Void)?

    /// Current bus pirate state
    public var state: BusPirateStates {
        didSet {
            if state != oldValue {
//                print("Transitioning from \(oldValue) to \(state)")
                stateChangeCallback?(oldValue, state)
            }
        }
    }
    public var timer: Timer?

    /// Instance of the serial port object
    internal let serialPort: SerialDevice

    /// Method that handles new bytes that come in from the serial port
    private func recieve(buffer: RingBuffer<UInt8>) {
        var newState = state

        // To ensure that we catch cases where the device has changed modes,
        // we'll get a copy of the current serial buffer in a non-destructive
        // way, and check if it contains any of the mode strings
        let ndBytes = buffer.getData(maxCount: 2048, destructive: false)
        if let ndString = String(bytes: ndBytes, encoding: .utf8) {
//            print([UInt8](ndBytes))
            // If one of these matches, we want to consume the
            // contents of the buffer.  Chances are that this is just one of
            // the strings above.  It's unlikely that there will be more,
            // because this protocol is command/response.
            switch ndString {
            case "BBIO1":
                newState = .bitbang
                buffer.discardContents()
            case "SPI1":
                newState = .spi
                buffer.discardContents()
            case "I2C1":
                newState = .i2c
                buffer.discardContents()
            case "ART1":
                newState = .uart
                buffer.discardContents()
            case "1W01":
                newState = .w1
                buffer.discardContents()
            case "RAW1":
                newState = .raw
                buffer.discardContents()
            default: break
            }
        }

        switch state {

        case .pinStatePending:
            if let byte = buffer.getData() {
                let pins = BusPirateOutputPins(rawValue: byte)
                newState = .pinStateResponse(pins)
            } else {
                newState = .unknown
            }

        case .pinMaskPending:
            if let byte = buffer.getData() {
                let pins = BusPirateInputPins(rawValue: byte)
                newState = .pinMaskResponse(pins)
            } else {
                newState = .unknown
            }

        case .i2cPending:
            let bytes: [UInt8] = buffer.getData()
            if let byte = bytes.last, byte == 0x01 { newState = .i2c }

        case .i2cPendingRead:
            let bytes: [UInt8] = buffer.getData()
            if let byte = bytes.last { newState = .i2cByteAvailable(byte) }

        case let .i2cWritePending(count):
            if let response = serialPort.rxBuffer.getData() {
                if response == 0x01 {
                    newState = .i2cWritingData(count, [])
                } else {
                    newState = .unknown
                }
            }

        // When we're writing data, the device may send a ACK/NACk to any of
        // the bytes sent.  So, we could get a number of bytes from the serial
        // port.  If we got more than one back, we need to process those returns
        // into ACKs/NACKs, and decrement the counter.
        case let .i2cWritingData(count, acks):
            let bytes: [UInt8] = buffer.getData()

            let newAcks = bytes.map({$0 == 0x00 ? true : false})
            let remaining = count - newAcks.count

            if remaining == 0 {
                newState = .i2cWriteComplete(acks + newAcks)
            } else {
                newState = .i2cWritingData(remaining, acks + newAcks)
            }

        case let .i2cWriteReadPending(count):
            if let byte = buffer.getData() {
                if byte == 0x01 {
                    newState = .i2cWriteRead(count, Data())
                } else {
                    newState = .i2c
                }
            }

        case let .i2cWriteRead(remaining, data):
            let bytes: [UInt8] = buffer.getData()

            let newData = data + bytes
            let newRemaining = remaining - bytes.count

            if newRemaining == 0 {
                newState = .i2cDataAvailable(newData)
            } else {
                newState = .i2cWriteRead(newRemaining, newData)
            }

        default: break
        }

        self.state = newState
    }

    public init(serialPort: SerialDevice) throws {
        self.state = .unknown

        self.serialPort = serialPort

        // Up to 21 times, send 0x00 to the device,
        // and wait for `BBIO1` to be returned
        var bytes = [UInt8]()
        for _ in 0 ..< 21 {
            guard self.state == .unknown else { break }
            try? serialPort.send(byte: 0x00)

            while let byte = try? serialPort.readByte(timeout: 0.1) {
                bytes.append(byte)
            }

            if bytes.count >= 5 {
                if String(bytes: bytes, encoding: .utf8)!.contains("BBIO1") {
                    self.state = .bitbang
                    break
                }
            }
        }

        if self.state != .bitbang {
            throw BusPirateError.invalidMode
        }

        serialPort.rxBuffer.discardContents()
        self.serialPort.receiveCallback = self.recieve
        self.serialPort.beginListening()
    }

    /// Set the pin states of the BusPirate where available.  Most modes allow for some pins to be set
    /// outside the context of the mode itself.
    /// - Parameter pins: BusPiratePins option set of the pins that should be set to logic "high"
    /// - Throws: May rethrow an error from the serial port, or an invalid mode error if this isn't supported
    public func setPinState(_ pins: BusPirateOutputPins) -> Promise<BusPirateStates> {
        return Promise { seal in
            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .pinStatePending, .i2cPending: return
                case .pinStateResponse, .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                let commandByte: UInt8

                self.acknowledge()
                switch state {
                case .i2c:
                    commandByte = 0x40 | pins.compactValue
                    self.state = .i2cPending
                case .spi, .uart, .w1, .raw:
                    commandByte = 0x40 | pins.compactValue
                    self.state = .pinStatePending
                case .bitbang:
                    commandByte = 0x80 | pins.rawValue
                    self.state = .pinStatePending
                default:
                    seal.reject(BusPirateError.invalidMode)
                    return
                }

                try serialPort.send(byte: commandByte)
            } catch {
                seal.reject(error)
            }
        }
    }

    public func setPinMask(_ pins: BusPirateInputPins) -> Promise<BusPirateStates> {
        return Promise { seal in
            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .pinMaskPending: return
                case .pinMaskResponse:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            do {
                let commandByte: UInt8

                switch state {
                case .bitbang:
                    commandByte = 0x40 | pins.rawValue
                default:
                    seal.reject(BusPirateError.invalidMode)
                    return
                }

                try serialPort.send(byte: commandByte)
                self.state = .pinMaskPending
            } catch {
                seal.reject(error)
            }

        }
    }

    /// The acknowledge function's job is to move from a state where data is presented to the
    /// user code into an idle state.  If a function that expects the idle state is called, and the
    /// state machine is in a presentation state, it'll move into idle before operation.  It's possible
    /// to lose data if it's not stored before calling a subsequent method.
    public func acknowledge() {
        switch state {
        case .i2cWriteComplete, .i2cDataAvailable, .i2cByteAvailable:
            state = .i2c
        case .pinStateResponse:
            state = .bitbang
        default:
            return
        }
    }

    /// Transition to the i2c mode.  The device must already be in the `bitbang` mode prior to selecting
    /// i2c mode.  Possible errors include:
    /// `BusPirateError.deviceBusy`, `BusPirateError.invalidMode`, and any low-level
    /// serial error.
    /// - Returns: Promise for the completion of transitioning to i2c mode.
    public func toI2c() -> Promise<BusPirateStates> {
        return Promise { seal in
            // Make sure there are no pending actions
            self.acknowledge()

            // Short-circuit if we're already in the desired state
            if self.state == .i2c {
                seal.fulfill(.i2c)
                return
            }

            // Ensure that we're in an appropriate mode
            guard self.state == .bitbang else {
                seal.reject(BusPirateError.invalidMode)
                return
            }

            // Ensure that the device isn't busy
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            // Make sure that we get notified by the serial port of any responses
            self.stateChangeCallback = { oldState, newState in
                switch newState {
                case .i2c:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.fulfill(newState)
                default:
                    self.stateChangeCallback = nil
                    self.operationLock.unlock()
                    seal.reject(BusPirateError.unexpectedResult)
                }
            }

            // Send the actual command to enter i2c mode
            do {
                try serialPort.send(byte: 0x02)
            } catch {
                operationLock.unlock()
                seal.reject(error)
                return
            }
        }
    }
}
