// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BusPirate",
    products: [
        .library(
            name: "BusPirate",
            targets: ["BusPirate"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:racepointenergy/swiftlibs/SerialHandler.git", .branch("trunk")),
        .package(url: "git@gitlab.com:racepointenergy/swiftlibs/RingBuffer.git",    .branch("trunk")),
        .package(url: "https://github.com/mxcl/PromiseKit.git",                     from: .init(6, 0, 0))

    ],
    targets: [
        .target(
            name: "BusPirate",
            dependencies: [
                "SerialHandler",
                "RingBuffer",
                "PromiseKit"
            ]
        ),
        .testTarget(
            name: "BusPirateTests",
            dependencies: ["BusPirate"])
    ]
)
