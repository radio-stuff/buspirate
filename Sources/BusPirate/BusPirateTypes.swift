//
//  BusPirateTypes.swift
//  BusPirate
//
//  Created by William Dillon on 8/3/20.
//

import Foundation

/// This enumeration maintains the state of the BusPirate peripheral.  These are maintained
/// according to the traffic of the serial port.  Each time a byte is received on the port, there
/// is an opportunity to transition between states.  This isn't a "pure" state machine, in that
/// some states also may contain additional information.  For example, an i2c transfer state
/// may contain the number of bytes expected, and the contents of the bytes received so far.
public enum BusPirateStates: Equatable {
    /// The state of the bus pirate is unknown.  This is the initial state of the machine, and should
    /// resolve to a known state once the reset command is sent to the peripheral.  To transition away
    /// from "unknown" mode is to send `0x00` to the BusPirate at least 20 times.
    case unknown
    /// BitBang mode is that "home" mode of the BusPirate.  Transitions to every other peripheral
    /// mode must originate at bitbang mode.
    case bitbang
    /// SetPinState is pending
    case pinStatePending
    /// Returned pin state result
    case pinStateResponse(BusPirateOutputPins)
    /// SetPinMask is pendinf
    case pinMaskPending
    /// Return result from pin mask (current state of pins)
    case pinMaskResponse(BusPirateInputPins)

    /// Binary SPI mode
    case spi

    /// Binary I2C mode
    case i2c
    /// an i2c command has been sent, and hasn't been acknodledged yet
    case i2cPending
    /// an i2c byte read is pending.  Once the byte is returned, send a ACK or a NACK
    case i2cPendingRead
    /// an i2c byte is available, send a ACK or NACK to return to the .i2c state
    case i2cByteAvailable(UInt8)
    /// When writing data, the write command itself will cause an acknowledgement.  So, we have to track
    /// that state
    case i2cWritePending(Int)
    /// The i2c device is having data written to it, the value is the number of bytes left to be acknowledged
    /// and the boolean array is the `ACK`/`NACK`s that's we've already received
    case i2cWritingData(Int, [Bool])
    /// The i2c device completed reading the data, the array is the progression of
    /// [ACK]/[NACK]s received from the device
    case i2cWriteComplete([Bool])
    /// We're waiting for the acknowledgement of the write from the device.  If we get `0x00`, then we
    /// know that we got a `NACK` from the peripheral.  Otherwise, we'll get a `0x01`.  If we get `0x00`,
    /// then we should return to the `.i2c` idle state.  The `Int` argument contains the number of bytes
    /// we're expecting
    case i2cWriteReadPending(Int)
    /// The i2c device is in the middle of a write-then-read command.  The `Data` argument is the current
    /// contents of the data received so far, and the `Int` argument represents the bytes we're waiting for.
    case i2cWriteRead(Int, Data)
    /// There is data available from the i2c peripheral
    case i2cDataAvailable(Data)

    /// Binary UART mode
    case uart
    /// Binary 1-wire mode
    case w1
    /// Binary raw-wire mode
    case raw
    /// OpenOCD JTAG mode (Not implemented)
    case openOCD
    /// Continuous voltage probe,  Once in this mode, the Bus Pirate will continuously measure the voltage
    /// on the ADC pin, and send the results.  In this mode, the callback provided to the method to enter
    /// the mode will be called for every update.
    case voltage
}

/// This struct provides means to specify which pins of the bus pirate should be set or cleared.  Unfortunately,
/// the order of pins in the command for bitbang mode is different than every other mode, so we can't use
/// the same ordering for all of them.  In all the modes other than bitbang, the lower four bits of the command
/// byte represent `power`, `pullup`, `aux`, and `cs`, in that order.  In bitbang mode, that order is
/// `power`, `pullup`, `aux`, `mosi`, `clk`, `miso`, `cs`.  So, we'll keep the pins in the order
/// used in bitbang mode, and fix it for use in other modes.
public struct BusPirateOutputPins: OptionSet {
    public let rawValue: UInt8

    public init(rawValue: UInt8) {
        self.rawValue = rawValue
    }

    /// Fix the value for non-bitbang modes by shifting the hitgher bits right by three, and restoring the
    /// `cs` pin back to where it belongs
    public var compactValue: UInt8 {
        return (rawValue >> 3 & 0x0E) | (rawValue & 0x01)
    }

    public static let power   = BusPirateOutputPins(rawValue: 0x40)
    public static let pullups = BusPirateOutputPins(rawValue: 0x20)
    public static let aux     = BusPirateOutputPins(rawValue: 0x10)
    public static let mosi    = BusPirateOutputPins(rawValue: 0x08)
    public static let clk     = BusPirateOutputPins(rawValue: 0x04)
    public static let miso    = BusPirateOutputPins(rawValue: 0x02)
    public static let cs      = BusPirateOutputPins(rawValue: 0x01)
}

/// This struct encapsulates the states of the input pins, and also allows the pin directions to be set in
/// bitbang mode.
public struct BusPirateInputPins: OptionSet {
    public let rawValue: UInt8

    public init(rawValue: UInt8) {
        self.rawValue = rawValue
    }

    public static let aux     = BusPirateInputPins(rawValue: 0x10)
    public static let mosi    = BusPirateInputPins(rawValue: 0x08)
    public static let clk     = BusPirateInputPins(rawValue: 0x04)
    public static let miso    = BusPirateInputPins(rawValue: 0x02)
    public static let cs      = BusPirateInputPins(rawValue: 0x01)
}

public enum BusPirateError: Error {
    case invalidMode
    case notYetImplemented
    case invalidLength
    case unexpectedResult
    case deviceBusy
}
