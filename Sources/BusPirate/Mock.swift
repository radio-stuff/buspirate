//
//  Mock.swift
//  BusPirate
//
//  Created by William Dillon on 8/2/20.
//

import Foundation
import RingBuffer
import PromiseKit

enum BusPirateMockError: Error {
    /// Data needed to respond to a command is missing.  Ensure that there's a state in stateBuffer
    /// before calling methods that expect a data response.
    case missingData
}

class BusPirateMock: BusPirateProtocol {
    private var stateBuffer: RingBuffer<BusPirateStates>

    public func add(state: BusPirateStates) {
        let added = stateBuffer.append(state)
        assert(added > 0)
    }

    public var state: BusPirateStates = .unknown {
        didSet {
            if state != oldValue {
                print("Transitioning from \(oldValue) to \(state)")
                stateChangeCallback?(oldValue, state)
            }
        }
    }

    private var operationLock = NSLock()
    private var stateChangeCallback: ((BusPirateStates, BusPirateStates) -> Void)?

    public func acknowledge() {
        switch state {
        case .i2cWriteComplete, .i2cDataAvailable:
            self.state = .i2c
        default:
            return
        }
    }

    init(bufferLength length: Int = 2048) {
        stateBuffer = RingBuffer<BusPirateStates>.init(size: length)
    }

    func setPinState(_ pins: BusPirateOutputPins) -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            DispatchQueue.global().async {
                self.state = .bitbang
                self.operationLock.unlock()
                seal.fulfill(.bitbang)
            }
        }
    }

    func setPinMask(_ pins: BusPirateInputPins) -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            DispatchQueue.global().async {
                self.state = .bitbang
                self.operationLock.unlock()
                seal.fulfill(.bitbang)
            }
        }
    }

    func toI2c() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cSetSpeed(speed: BusPirateI2CSpeed) -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cStartBit() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cStopBit() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cReadByte() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard let nextState = stateBuffer.getData() else {
                assertionFailure("Missing sample data in mock")
                return
            }

            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = nextState
                self.operationLock.unlock()
                seal.fulfill(nextState)
            }
        }
    }

    func i2cSendACK() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cSendNACK() -> Promise<BusPirateStates> {
        return Promise { seal in
            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cPending

            DispatchQueue.global().async {
                self.state = .i2c
                self.operationLock.unlock()
                seal.fulfill(.i2c)
            }
        }
    }

    func i2cWriteDataLL(_ data: Data) -> Promise<BusPirateStates> {
        return Promise { seal in
            guard let nextState = stateBuffer.getData() else {
                assertionFailure("Missing sample data in mock")
                return
            }

            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cWritePending(data.count)

            DispatchQueue.global().async {
                self.state = nextState
                self.operationLock.unlock()
                seal.fulfill(nextState)
            }
        }
    }

    func i2cWriteRead(writeData data: Data, count: Int) -> Promise<BusPirateStates> {
        return Promise { seal in
            let nextStates: [BusPirateStates] = stateBuffer.getData()

            guard nextStates.count > 0 else {
                assertionFailure("Missing sample data in mock")
                return
            }

            guard operationLock.try() else {
                seal.reject(BusPirateError.deviceBusy)
                return
            }

            state = .i2cWritePending(data.count)

            for nextState in nextStates {
                DispatchQueue.global().async {
                    self.state = nextState
                }
            }

            self.operationLock.unlock()
            seal.fulfill(nextStates.last!)
        }
    }
}
