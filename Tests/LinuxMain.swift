import XCTest

import BusPirateTests

var tests = [XCTestCaseEntry]()
tests += BusPirateTests.allTests()
XCTMain(tests)
