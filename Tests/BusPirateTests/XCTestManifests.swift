import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(BusPirate_swiftTests.allTests)
    ]
}
#endif
