//
//  BusPirateProtocol.swift
//  BusPirate
//
//  Created by William Dillon on 8/3/20.
//

import Foundation
import PromiseKit

protocol BusPirateProtocol {
    var state: BusPirateStates { get }

    /// The acknowledge function's job is to move from a state where data is presented to the
    /// user code into an idle state.  If a function that expects the idle state is called, and the
    /// state machine is in a presentation state, it'll move into idle before operation.  It's possible
    /// to lose data if it's not stored before calling a subsequent method.
    func acknowledge()

    // MARK: Bitbang methods
    /// Set the pin states of the BusPirate where available.  Most modes allow for some pins to be set
    /// outside the context of the mode itself.
    /// - Parameter pins: BusPiratePins option set of the pins that should be set to logic "high"
    /// - Throws: May rethrow an error from the serial port, or an invalid mode error if this isn't supported
    func setPinState(_ pins: BusPirateOutputPins) -> Promise<BusPirateStates>
    func setPinMask(_ pins: BusPirateInputPins) -> Promise<BusPirateStates>

    // MARK: I2C methods
    func toI2c() -> Promise<BusPirateStates>
    func i2cSetSpeed(speed: BusPirateI2CSpeed) -> Promise<BusPirateStates>
    /// Send an I2C start bit. Responds 0x01.
    /// - Throws: SerialHandler error, if one occured
    func i2cStartBit() -> Promise<BusPirateStates>
    /// Send an I2C stop bit. Responds 0x01.
    /// - Throws: SerialHandler error, if one occured
    func i2cStopBit() -> Promise<BusPirateStates>
    /// Read one byte from the i2c bus
    /// - Throws: SerialHandler error, if encountered
    func i2cReadByte() -> Promise<BusPirateStates>
    /// Send an i2c ACK
    /// - Throws: SerialHandler error, or `BusPirateError.invalidMode`
    func i2cSendACK() -> Promise<BusPirateStates>
    /// Send an i2c NACK
    /// - Throws: SerialHandler error, or `BusPirateError.invalidMode`
    func i2cSendNACK() -> Promise<BusPirateStates>
    func i2cWriteDataLL(_ data: Data) -> Promise<BusPirateStates>
    func i2cWriteRead(writeData data: Data, count: Int) -> Promise<BusPirateStates>
}
