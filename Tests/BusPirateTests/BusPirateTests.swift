import XCTest
import SerialHandler
import RingBuffer
import PromiseKit
@testable import BusPirate

extension String {
    var bytes: [UInt8] {
        return [UInt8](self.data(using: .utf8)!)
    }
}

final class BusPirate_swiftTests: XCTestCase {
//    func testModeChanges() {
//        do {
//            let devices = try UARTConfiguration.availablePorts()
//            let tryConfig = devices.reduce(nil) { last, next -> UARTConfiguration? in
//                return next.path == "/dev/tty.usbserial-A700eDR5" ? next : last
//            }
//
//            guard var config = tryConfig else {
//                XCTAssert(false, "Unable to load port")
//                return
//            }
//
//            config.baud = 115200
//
//            let serialDevice = try SerialUART(config: config)
//            let busPirate = try BusPirate(serialPort: serialDevice)
//
//            var promise = expectation(description: "enter binary mode")
//
//            busPirate.stateChangeCallback = { _, newState in
//                if newState == .bitbang { promise.fulfill() }
//            }
//
//            wait(for: [promise], timeout: 10)
//
//            // Now that we're in bitbang mode, try switching to i2c mode
//            promise = expectation(description: "switch to i2c")
//            busPirate.stateChangeCallback = { _, newState in
//                if newState == .i2c { promise.fulfill() }
//            }
//
//            busPirate.toI2c().done { state in
//
//            }.catch{ error in
//                print(dump(error))
//                XCTAssert(false)
//            }
//
//            wait(for: [promise], timeout: 10)
//
//            // Try sending an i2c start
//            let promise1 = expectation(description: "Go to pending")
//            let promise2 = expectation(description: "Get response")
//            busPirate.stateChangeCallback = { _, newState in
//                if newState == .i2cPending { promise1.fulfill() }
//                if newState == .i2c { promise2.fulfill() }
//            }
//
//            busPirate.i2cStartBit()
//
//            wait(for: [promise1, promise2], timeout: 10)
//        } catch {
//            print(dump(error))
//            XCTAssert(false)
//        }
//    }

    func testAPIMockI2C() {
        let busPirate = BusPirateMock(bufferLength: 16)
        busPirate.state = .i2c

        for i in 0 ..< i2cTests.count {
            busPirate.acknowledge()
            let (closure, _, _, _, finalState) = i2cTests[i]

            let promise = expectation(description: "Final")

            switch finalState {
            case .i2cByteAvailable:
                busPirate.add(state: finalState)
            case .i2cWriteComplete:
                busPirate.add(state: finalState)
            case .i2cDataAvailable:
                busPirate.add(state: finalState)
            default: break
            }

            closure(busPirate)?.done { state in
                if state == finalState {
                    promise.fulfill()
                }
            }.catch { error in
                print(dump(error))
                XCTAssert(false)
            }

            waitForExpectations(timeout: 10) { error in
                if let error = error {
                    XCTAssert(false, "Error transitioning to \(finalState) in test \(i): \(dump(error))")
                }
            }
        }
    }

    func testSerialMockI2C() {
        var step = 0
        do {
            let serialMock = try SerialMock()
            serialMock.rxBuffer = RingBuffer<UInt8>(size: 8192)
            serialMock.txBuffer = RingBuffer<UInt8>(size: 8192)
            // Prepare the serial mock for the data init is expecting
            _ = serialMock.rxBuffer.append("BBIO1".bytes)
            let busPirate = try BusPirate(serialPort: serialMock)
            // Discard the byte that was sent to enter bitbang more
            serialMock.txBuffer.discardContents()
            busPirate.state = .i2c

            for i in 0 ..< i2cTests.count {
                step = i
                let (closure, tx, rx, _, finalState) = i2cTests[i]

                let promise = expectation(description: "Final")

                busPirate.acknowledge()
                closure(busPirate)?.done { state in
                    if state == finalState {
                        promise.fulfill()
                    }
                }.catch { error in
                    print(dump(error))
                    XCTAssert(false)
                }

                let txData: [UInt8] = serialMock.txBuffer.getData()
                XCTAssertEqual(txData, tx)

                if tx.count > 0 {
                    serialMock.add(data: Data(rx), addMethod: .byByte)
                } else {
                    promise.fulfill()
                }

                waitForExpectations(timeout: 10) { error in
                    if let error = error {
                        XCTAssert(false, "Error transitioning to \(finalState) in test \(i): \(dump(error))")
                    }
                }
            }

        } catch {
            print("Error at step \(step): \(dump(error))")
            XCTAssert(false)
        }
    }

    func testPinDefinitions() {
        let samples: [(BusPirateOutputPins, UInt8, UInt8)] = [
            (BusPirateOutputPins(rawValue: 0x40), 0x40, 0x08), // Power
            (BusPirateOutputPins(rawValue: 0x20), 0x20, 0x04), // Pullups
            (BusPirateOutputPins(rawValue: 0x10), 0x10, 0x02), // Aux
            (BusPirateOutputPins(rawValue: 0x08), 0x08, 0x00), // MISO
            (BusPirateOutputPins(rawValue: 0x04), 0x04, 0x00), // CLK
            (BusPirateOutputPins(rawValue: 0x02), 0x02, 0x00), // MOSI
            (BusPirateOutputPins(rawValue: 0x01), 0x01, 0x01), // CS
            (BusPirateOutputPins(rawValue: 0x00), 0x00, 0x00)  // nothing
        ]

        for sample in samples {
            XCTAssertEqual(sample.0.rawValue,     sample.1)
            XCTAssertEqual(sample.0.compactValue, sample.2)
        }
    }

    static var allTests = [
        ("testPinDefinitions", testPinDefinitions),
        ("testSerialMockI2C",  testSerialMockI2C),
        ("testAPIMockI2C",     testAPIMockI2C)
    ]
}

let sampleData: [UInt8] = [
    0xF0, 0xE1, 0xD2, 0xC3, 0xB4, 0xA5, 0x96, 0x87,
    0x78, 0x69, 0x5A, 0x4B, 0x3C, 0x2D, 0x1E, 0x0F
]

let i2cTests: [(((BusPirateProtocol) -> Promise<BusPirateStates>?), [UInt8], [UInt8], BusPirateStates, BusPirateStates)] = [
    ({$0.i2cSetSpeed(speed: .khz400)}, [0x63], [0x01],       .i2cPending, .i2c),
    ({$0.i2cSetSpeed(speed: .khz100)}, [0x62], [0x01],       .i2cPending, .i2c),
    ({$0.i2cSetSpeed(speed: .khz50 )}, [0x61], [0x01],       .i2cPending, .i2c),
    ({$0.i2cSetSpeed(speed: .khz5  )}, [0x60], [0x01],       .i2cPending, .i2c),
    ({$0.i2cStartBit()},               [0x02], [0x01],       .i2cPending, .i2c),
    ({$0.i2cStopBit()},                [0x03], [0x01],       .i2cPending, .i2c),
    ({$0.i2cReadByte()},               [0x04], [0x55],       .i2cPendingRead, .i2cByteAvailable(0x55)),
    ({$0.i2cSendACK()},                [0x06], [0x01],       .i2cPending, .i2c),
    ({$0.i2cReadByte()},               [0x04], [0xAA],       .i2cPendingRead, .i2cByteAvailable(0xAA)),
    ({$0.i2cSendNACK()},               [0x07], [0x01],       .i2cPending, .i2c),
    (
        {$0.i2cWriteDataLL(Data(sampleData))},
        [0x1F] + sampleData,
        [0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01],
        .i2cWritePending(16), .i2cWriteComplete([
            true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, false
        ])
    ),
    (
        {$0.i2cWriteRead(writeData: Data(repeating: 0xAA, count: 4096), count: 4096)},
        [0x08, 0x10, 0x00, 0x10, 0x00] + Data(repeating: 0xAA, count: 4096),
        [0x01] + Data(repeating: 0x55, count: 4096),
        .i2cWriteReadPending(4096), .i2cDataAvailable(Data(repeating: 0x55, count: 4096)))
]
